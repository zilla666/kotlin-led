package com.example.makeme

import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_controll.*
import java.io.IOException
import java.util.*

class ControllActivity : AppCompatActivity() {
    companion object {
        var m_myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        var m_bluetoothSocket: BluetoothSocket? = null
        lateinit var m_progress: ProgressDialog
        lateinit var m_bluetoothAdapter: BluetoothAdapter
        var m_isConnected: Boolean = false
        lateinit var m_address: String
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        //test git
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_controll)
        m_address = intent.getStringExtra(MainActivity.EXTRA_ADDRESS).toString()
        ConnectToDevice(this).execute()

        control_upall.setOnTouchListener {  view, motionEvent ->
        when (motionEvent.action){
            MotionEvent.ACTION_DOWN -> {
                sendCommand("0")
                true
            }
            MotionEvent.ACTION_UP -> {
                sendCommand("1")
                true
            }
            else -> false
        }
        }
        control_frontup.setOnTouchListener { view, motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_DOWN -> {
                    sendCommand("2")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    sendCommand("3")
                    true
                }
                else -> false
            }
        }
        control_frontdown.setOnTouchListener { view, motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_DOWN -> {
                    sendCommand("4")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    sendCommand("5")
                    true
                }
                else -> false
            }

        }
        control_rearup.setOnTouchListener { view, motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_DOWN -> {
                    sendCommand("6")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    sendCommand("7")
                    true
                }
                else -> false
            }
        }
        control_reardown.setOnTouchListener { view, motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_DOWN -> {
                    sendCommand("8")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    sendCommand("9")
                    true
                }
                else -> false
            }
        }
        control_downall.setOnTouchListener { view, motionEvent ->
            when (motionEvent.action){
                MotionEvent.ACTION_DOWN -> {
                    sendCommand("10")
                    true
                }
                MotionEvent.ACTION_UP -> {
                    sendCommand("11")
                    true
                }
                else -> false
            }
        }
      // control_led_off.setOnClickListener {  }
        //control_led_disconnect.setOnClickListener { disconnect() }
    }
    private fun sendCommand(input: String) {
        if (m_bluetoothSocket != null) {
            try{
                m_bluetoothSocket!!.outputStream.write(input.toByteArray())
            } catch(e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun disconnect() {
        if (m_bluetoothSocket != null) {
            try {
                m_bluetoothSocket!!.close()
                m_bluetoothSocket = null
                m_isConnected = false
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        finish()
    }

    private class ConnectToDevice(c: Context) : AsyncTask <Void, Void, String>() {
        private var connectSuccess: Boolean = true
        private val context: Context

        init {
            this.context = c
        }

        override fun onPreExecute() {
            super.onPreExecute()
            m_progress = ProgressDialog.show(context, "Connecting...", "please wait")
        }

        override fun doInBackground(vararg p0: Void?): String? {
            try {
                if (m_bluetoothSocket == null || !m_isConnected) {
                    m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                    val device: BluetoothDevice = m_bluetoothAdapter.getRemoteDevice(m_address)
                    m_bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(m_myUUID)
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
                    m_bluetoothSocket!!.connect()
                }
            } catch (e: IOException) {
                connectSuccess = false
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (!connectSuccess) {
                Log.i("data", "couldn't connect")
            } else {
                m_isConnected = true
            }
            m_progress.dismiss()
        }
    }

}